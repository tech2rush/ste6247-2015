#ifndef MYCURVE_H
#define MYCURVE_H

//#include <parametrics/gmpcurve>
#include <parametrics/gmpcurve>
//#include <parametrics/gmpsurf>

class MyCurve: public GMlib::PCurve<float,3>
{
    GM_SCENEOBJECT(MyCurve)
public:
    MyCurve( float size = 5.0);
    MyCurve( const MyCurve& copy );

    ~MyCurve();

    //bool isClosed() const;

protected:
    void eval(float t, int d, bool l);
    float getEndP();
    float getStartP();
    bool isClosed() const {return true;}

    //GMlib::DVector<float> makeKnotVector(float start, float end, int n);

    GMlib::DVector<float> _knotVector;

    float _size;

};

#endif // MYCURVE_H
