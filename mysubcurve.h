#ifndef MYSUBCURVE_H
#define MYSUBCURVE_H

#include <parametrics/gmpcurve>

class MySubCurve: public GMlib::PCurve<float,3> {
    GM_SCENEOBJECT(MySubCurve)
public:
    MySubCurve( PCurve<float, 3>* curve, float start, float end, float t);
    MySubCurve( const MySubCurve& copy);
    ~MySubCurve(){};

protected:
    PCurve<float,3>* _curve;
    float _start;
    float _end;
    float _t;
    GMlib::Vector<float,3> _translationVector;

    void eval(float t, int d, bool l=false);
    float getStartP();
    float getEndP();
    bool isClosed() const;

};
#endif//MYSUBCURVE_H
