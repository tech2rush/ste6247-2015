#ifndef MYCURVESURFACE_H
#define MYCURVESURFACE_H

#include <parametrics/gmpsurf>
#include <parametrics/gmpcurve>

class MyCurveSurface : public GMlib::PSurf<float,3>
{
   GM_SCENEOBJECT(MyCurveSurface)
public:
   MyCurveSurface(GMlib::PCurve<float,3>* c, float v = 1.0f);//Curve, "radius"
   ~MyCurveSurface();

   bool isClosedU() const;
   bool isClosedV() const;

protected:

    void eval(float u, float v, int d1, int d2, bool lu, bool lv);
    float getEndPU();
    float getEndPV();
    float getStartPU();
    float getStartPV();

    bool _closedU = false;
    bool _closedV = false;

    GMlib::PCurve<float,3>* _c;//Curve
    float _v;//"Radius"
};

#endif//MYCURVESURFACE_H
