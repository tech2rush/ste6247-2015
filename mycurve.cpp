#include "mycurve.h"

MyCurve::MyCurve(float size)
{
   _size = size;
   _dm = GMlib::GM_DERIVATION_EXPLICIT;
  // _knotVector = makeKnotVector(0, 6.28318530, 6);

}

MyCurve::MyCurve(const MyCurve &copy) : PCurve<float,3>( copy )
{

}


MyCurve::~MyCurve()
{

}

void MyCurve::eval(float t, int d, bool l)
{
    this->_p.setDim(d + 1);

    this->_p[0][0] = _size * ( sin(t) + 0.5*cos(2*t) + 0.25*sin(4*t) );
    this->_p[0][1] = _size * ( cos(t) + 0.5*sin(2*t) + 0.25*cos(4*t) );
    this->_p[0][2] = 0;

    if( this->_dm == GMlib::GM_DERIVATION_EXPLICIT ) {

        if( d > 0 ) {

            this->_p[1][0] = _size * ( cos(t) - sin(2*t) + cos(4*t) );
            this->_p[1][1] = _size * (-sin(t) + cos(2*t) - sin(4*t) );
            this->_p[1][2] = _size;
        }

        if( d > 1 ) {

            this->_p[2][0] = _size * (-sin(t) - 2*cos(2*t) - 4*sin(4*t) );
            this->_p[2][1] = _size * (-cos(t) - 2*sin(2*t) - 4*cos(4*t) );
            this->_p[2][2] = _size;
        }
    }
}

float MyCurve::getStartP()
{
    //return second value of knot vector
    if (_knotVector.getDim() < 2) return 0;
    return _knotVector[1];
}

float MyCurve::getEndP()
{
    //return second to last value of knot vector
    if(_knotVector.getDim() < 3) return 6.28318530;
    return _knotVector[_knotVector.getDim()-2];
}
