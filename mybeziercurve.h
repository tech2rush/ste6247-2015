#ifndef MYBEZIERCURVE_H
#define MYBEZIERCURVE_H

#include <parametrics/gmpcurve>
#include "makematrix.h"

class MyBezierCurve : public GMlib::PCurve<float,3>
{
    GM_SCENEOBJECT(MyBezierCurve)

public:
    MyBezierCurve(const GMlib::DVector< GMlib::Vector<float,3> >& control_points, float start, float t, float end);
    MyBezierCurve( const MyBezierCurve& copy );
    ~MyBezierCurve(){}

    int getDegree() const;

    GMlib::DVector<GMlib::Vector<float, 3> > getControl_points() const;
    void setControl_points(const GMlib::DVector<GMlib::Vector<float, 3> > &control_points);

    bool isClosed() const;
    void setClosed(bool closed);

protected:
    GMlib::DVector< GMlib::Vector<float, 3> > _control_points;
    GMlib::DVector< GMlib::DMatrix<float> > _t;

    float _scale = 1;
    bool _closed = false;
    bool _pre_eval = false;

    void eval(float t, int d = 0, bool l = false);
    float getStartP();
    float getEndP();
};
#endif//MYBEZIERCURVE_H
