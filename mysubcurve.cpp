#include "mysubcurve.h"

MySubCurve::MySubCurve(PCurve<float, 3> *curve, float start, float end, float t)
{
    _dm =  GMlib::GM_DERIVATION_EXPLICIT;
    _curve = curve;
    _start = start;
    _end = end;
    _t = t;

    GMlib::DVector<GMlib::Vector<float,3> > tr = _curve->evaluateParent(_t,0);
    _translationVector = tr[0];
    this->translateParent(_translationVector);
}

MySubCurve::MySubCurve(const MySubCurve &copy)
{
    _curve = copy._curve;
    _start = copy._start;
    _end = copy._end;
    _t = copy._t;
    _translationVector = copy._translationVector;
}

void MySubCurve::eval(float t, int d, bool l)
{
    this->_p = _curve->evaluateParent(t, d);
    this->_p[0] -= _translationVector;

}

float MySubCurve::getStartP()
{
    return _start;
}

float MySubCurve::getEndP()
{
    return _end;
}

bool MySubCurve::isClosed() const
{
   return false;
}
