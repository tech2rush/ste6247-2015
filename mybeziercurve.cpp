#include "mybeziercurve.h"

MyBezierCurve::MyBezierCurve(const GMlib::DVector< GMlib::Vector<float, 3> > &control_points, float start, float t, float end)
{
    GMlib::DMatrix<float> bernst_herm_poly;
    makeMatrix<float>(bernst_herm_poly, control_points.getDim()-1, (t-start)/(end-start), 1.0/(end-start) );
    bernst_herm_poly.invert();
    _control_points = bernst_herm_poly * control_points;

    for( int i = 0; i < control_points.getDim(); i++)
    {
        _control_points[i] -= control_points(0);
    }
    translateParent( control_points(0));
}

MyBezierCurve::MyBezierCurve(const MyBezierCurve &copy)
{
    _control_points = copy._control_points;
}

int MyBezierCurve::getDegree() const
{
    return _control_points.getDim() -1;
}

GMlib::DVector<GMlib::Vector<float, 3> > MyBezierCurve::getControl_points() const
{
    return _control_points;
}

void MyBezierCurve::setControl_points(const GMlib::DVector<GMlib::Vector<float, 3> > &control_points)
{
   _control_points = control_points;
}

bool MyBezierCurve::isClosed() const
{
    return _closed;
}

void MyBezierCurve::setClosed(bool closed)
{
    _closed = closed;
}

void MyBezierCurve::eval(float t, int d, bool l)
{
    GMlib::DMatrix<float> bernst_herm_poly;
    makeMatrix(bernst_herm_poly, getDegree(), t, _scale );

    this->_p = bernst_herm_poly * _control_points;
}

float MyBezierCurve::getStartP()
{
    return 0;

}

float MyBezierCurve::getEndP()
{
    return 1;
}
