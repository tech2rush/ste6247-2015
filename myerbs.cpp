#include "myerbs.h"
#include "mysubcurve.h"
#include "mybeziercurve.h"

#include <parametrics/gmerbsevaluator>
#include <parametrics/gmpbeziercurve>

MyERBSCurve::MyERBSCurve(MyERBSCurve::CURVE_TYPE type, PCurve<float, 3> *curve, int n, int d)
{

    _evaluator = new GMlib::ERBSEvaluator<long double>;

    if (curve->isClosed()) setClosed(true);
    if (isClosed()) n++;

    _knotVector = makeKnotVector( curve->getParStart(), curve->getParEnd(), n);

    std::cout << _knotVector << std::endl;

    _curves.setDim(n);
    for(int i = 1; i < n; i++)
    {
        _curves[i-1] = makeLocal(type, curve, _knotVector[i-1], _knotVector[i], _knotVector[i+1], d);
        insertLocal(_curves[i-1]);
    }

    if (isClosed()) _curves[n-1] = _curves[0];
    else
    {
        _curves[n-1] = makeLocal(type, curve, _knotVector[n-1], _knotVector[n], _knotVector[n+1], d);
        insertLocal(_curves[n-1]);
    }
}

MyERBSCurve::MyERBSCurve(const MyERBSCurve &copy)
{

}

bool MyERBSCurve::isClosed() const
{
    return _isClosed;

}

void MyERBSCurve::setClosed(bool closed)
{
    _isClosed = closed;
}

void MyERBSCurve::insertLocal(PCurve<float, 3> *localCurve)
{
    localCurve->toggleDefaultVisualizer();
    localCurve->replot(_resLocalCurves,1);
    localCurve->setColor(GMlib::GMcolor::Blue);
    localCurve->setVisible(true);
    localCurve->setCollapsed(false);
    this->insert( localCurve );
}

GMlib::PCurve<float, 3> *MyERBSCurve::makeLocal(MyERBSCurve::CURVE_TYPE type, PCurve<float, 3> *curve, float start, float t, float end, int d)
{
    switch(type)
    {
    case SUB_CURVE:
        return new MySubCurve(curve, start, end, t);
    case BEZIERCURVE:
        //return new GMlib::PBezierCurve<float>(curve->evaluateParent(t,d),start, t, end);
        return new MyBezierCurve(curve->evaluateParent(t,d),start, t, end);
    }
}

void MyERBSCurve::eval(float t, int d, bool l)
{

    int k;
    // Find knot
    for ( k = 1; k < _knotVector.getDim()-2; ++k)
        if (t < _knotVector[k+1]) break;
    //if (k > _knotVector.getDim()-3) k = _knotVector.getDim() -3;

    //First curve
    GMlib::DVector< GMlib::Vector<float,3> > c0 = _curves[k-1]->evaluateParent( mapToLocal(t, k), d);

    if (std::abs(t - _knotVector[k])  < 1e-5 )
    {
        _p = c0;
        return;
    }

    //Second curve
    GMlib::DVector< GMlib::Vector<float,3> > c1 = _curves[k]->evaluateParent(mapToLocal(t,k+1),d);

    //Blend
    GMlib::DVector<float> B;
    getB(B,k,t,d);
    compBlend( d, B, c0, c1);
}

float MyERBSCurve::getStartP()
{
    if (_knotVector.getDim()>0) return _knotVector[1];
    else return 0;

}

float MyERBSCurve::getEndP()
{
    if (_knotVector.getDim() > 1) return _knotVector[_knotVector.getDim() -2];
    else return 0;
}

//Copy/paste
float MyERBSCurve::mapToLocal(float t, int tk)
{
    float c_start = _curves[tk-1]->getParStart();
    float c_delta = _curves[tk-1]->getParDelta();
    return c_start + (t - _knotVector[tk-1]) / (_knotVector[tk+1] - _knotVector[tk-1]) * c_delta;
}

//Copy/pasted
void MyERBSCurve::getB(GMlib::DVector<float> &B, int k, float t, int d)
{
    B.setDim(d+1);
    _evaluator->set(_knotVector[k], _knotVector[k+1] - _knotVector[k]);
    B[0] = 1 - (*_evaluator)(t);
    switch(d) {
      case 3: B[3] = float(0);
      case 2: B[2] = - _evaluator->getDer2();
      case 1: B[1] = - _evaluator->getDer1();
    }
}

//Copy/pasted
void MyERBSCurve::compBlend(int d,
                            const GMlib::DVector<float> &B,
                            GMlib::DVector<GMlib::Vector<float, 3> > &c0,
                            GMlib::DVector<GMlib::Vector<float, 3> > &c1)
{
    c0 -= c1;

    GMlib::DVector<float> a(d+1);
    for( int i = 0; i < B.getDim(); i++ )
    {
      // Compute the pascal triangle numbers
      a[i] = 1;
      for( int j = i-1; j > 0; j-- )
        a[j] += a[j-1];
      // Compute the sample position data
      for( int j = 0; j <= i; j++ )
        c1[i] += (a[j] * B(j)) * c0[i-j];
    }
    this->_p = c1;

}

void MyERBSCurve::localSimulate(double dt)
{

    _at += dt;//Accumulated time
    if (_at > M_2PI) _at-=M_2PI;//but limited to [0, 2Pi]

    //Rotate and translate local curves
    GMlib::DVector< GMlib::Vector<float,3> > tArray;

    auto localRotationVector = GMlib::Vector<float,3>(0.25f, 0.5f, 1.0f);
    //auto localRotationVector = GMlib::Vector<float,3>(0.0f, 0.0f, 1.0f);

    for(int i=0; i < _curves.getDim()-1; i++)
    {
        _curves[i]->rotate(dt, localRotationVector);
        //This really need to be moved outside and pregenerated
        GMlib::Vector<float,3> ce = _curves[i]->evaluateParent(_knotVector[i+1], 0)[0];
        tArray.append(ce/ce.getLength());
        GMlib::Vector<float,3> ce_normalized = ce/ce.getLength();

        _curves[i]->translateParent(0.02*sin(_at)* ce_normalized);
    }

    //_curves[0]->setColor(GMlib::GMcolor::Green);
    //_curves[1]->setColor(GMlib::GMcolor::Yellow);
    //_curves[2]->setColor(GMlib::GMcolor::Orange);
    //_curves[3]->setColor(GMlib::GMcolor::Black);

    this->replot(200,1);
}

GMlib::DVector<float> MyERBSCurve::makeKnotVector(float start, float end, int n)
{
    GMlib::DVector<float> dv;
    float currentKnotValue = start;
    float increment = (end-start) / (n -1);

    if(_isClosed) dv.append(start - increment);
    else dv.append(start);
/*
    while (currentKnotValue < end)
    {
        dv.append(currentKnotValue);
        currentKnotValue += increment;
    }
    */
    for (int i = 0; i < n; i++)
    {
        dv.append(currentKnotValue);
        currentKnotValue += increment;
    }
    if(_isClosed) dv.append(end + increment);
    else dv.append(end);

    std::cout << "Knot vector produced:\n";
    std::cout << dv << std::endl;
    return dv;
}
