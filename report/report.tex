
% Document class
\documentclass[11pt,a4paper]{article}


% Encoding
\usepackage[utf8]{inputenc}

% Ref and Misc
\usepackage{url}
\usepackage{cleveref}

% Graphics
\usepackage{graphicx}

% Algorithm
\usepackage{algorithm}
\usepackage{algpseudocode}

% Ams
\usepackage{amsmath,amssymb,amsthm}

% Source code
\usepackage{listings}



\title{STE6247 - Applied geometry and special effects}
\author{
  Gisle Jaran Granmo\\ \\
  Narvik University College, Narvik, Norway
}
\date{\today}



\begin{document}
\lstset{language=C++}

% Creates the title page
\maketitle


\begin{abstract}
This report details the project work done for the course STE6247 Applied geometry and special effects at NUC (Narvik University College). The project concerns the implementation of Expo-Rational B-Spline curves (ERBS) using both sub-curves and bezier curves as local curves. The local curves are subjected to affine transformations to illustrate their effect on the blended curve. As per assignment instructions, a brief coverage of tesselation theory is also included.
\end{abstract}

\section{Introduction}
The primary task for this project was the implementation of ERBS curves with support for both sub curves or bezier curves as local curves. To this end all three classes of curves and necessary functionality were to be created based on the in-house geometric modelling library, GMlib. In addition a self designed custom curve was to be created to showcase the implementation of the aforementioned curve classes. 

\subsection{GMlib}
GMlib is NUC's (Narvik University College) inhouse open source geometric modelling library. GMlibs primary focus is paramatric curves and surfaces, and the necessary methods to interact and visualize these, and is used for NUC's geometric modelling research. For testing purposes I have maintained class and method signature compability with existing classes to allow for easy testing of my own code by drop-in replacement of reference implementations.

\subsection{QT}
QT is cross platform widget and i/o library, originally designed by Trolltech of Norway. Aside from being cross platform and open source, it's main attraction is its model of loose coupling between objects, which allows objects to react to eachother's behaviour without knowledge of eachother.\\
For this project little direct work was done with QT as we were working from precoded sample code. 

\section{Parametric curves}
A parametric curve is defined by a parametric equation with one parameter, usually denoted t. For geometric modelling two or three dimensions is the most common, and in our case we will use two-dimensional curves in a three dimensional space. Techniques used in the implementations are based on \cite{laksaa:blend_2012} Blending technics for Curve and Surface constructions.

\subsection{Subcurve}
A subcurve is a section of a longer curve, that is it's start and end-points within the span of it's parent curve. It's our simplest kind of local curve and by far the easiest to implement. It's shape will always retain it's shape, as long as it's parent curve remains unchanged. It's by far the quickest and simplest local curve to implement.

\subsection{Bezier curves}
Bezier curves are defined by control points which allows them to be manipulated in ways simple subcurves can not. Using these control points and the Cox de Boor algorithm for Bernstein Hermite polynomials, we are able to create a continous from these points only.

\subsection{ERBS}
Expo rational b-splines are created by blending local parametric curves. In our project we will use subcurves and bezier curves, but other options are possible as long as they are parameterizable. By performing transformations on the local curves we illustrate how these local curves are connected by the ERBS blending algorithm.


\section{Project}


\subsection{Custom curve}

The custom curve that is used as a basis for testing the ERBS related classes is a two dimensional closed parametric curve, based on a simple circle with periodic distortions. The curve was primarily chosen due to its simplicity with easily recognizable parts and no crossing sections. \cref{fig::custom_curve}

\begin{equation}
\begin{array}{lcl}
x(t) = sin(t) + 0.5*cos(2*t) + 0.25*sin(4*t)\\
y(t) = cos(t) + 0.5*sin(2*t) + 0.25*cos(4*t)\\
z(t) = 0
\end{array}
\Bigg\}t \in [0,2\pi>
\end{equation}

\subsection{ERBS using subcurves}
By subdividing the original curve it can be manipulated by performing affine transformations on the local curves as shown in \cref{fig::custom_curve-animated} and \cref{fig::custom_curve-subcurves}. The curves in these screenshots are continually being transformed/deformed by rotating the local curves upon a local axis, then being translated to and from the centre of the ERBS in a sine motion. 
\begin{figure}[!ht]
\centering
\includegraphics[scale=0.5]{gfx/custom_curve}
\caption{Unmanipulated custom curve.}
\label{fig::custom_curve}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[scale=0.5]{gfx/custom_curve-animated}
\caption{Custom curve animated using local rotations and translations.}
\label{fig::custom_curve-animated}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[scale=0.5]{gfx/custom_curve-subcurves}
\caption{Custom curve animated with subcurves visible in blue.}
\label{fig::custom_curve-subcurves}
\end{figure}


\subsection{ERBS using bezier curves}
For this section the ERBS is best visualized using a simple circle as the base curve, as there was undesired behaviour when translating the bezier local curves. Contrast \cref{fig::circle-bezier-no-translate}, where there is only local rotation (which works as intended), with \cref{fig::circle-bezier-translation-error} where the local curves are drifting out from the center in an irregular manner, rather than expanding and contracting in a cyclic manner like the subcurves did. 
\begin{figure}[!ht]
\centering
\includegraphics[scale=0.5]{gfx/circle-bezier-no-translate}
\caption{Circle of bezier curves animated using local rotations only.}
\label{fig::circle-bezier-no-translate}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[scale=0.5]{gfx/circle-bezier-translation-error}
\caption{Circle of bezier curves exibiting undesired behaviour when translated.}
\label{fig::circle-bezier-translation-error}
\end{figure}


\section{Code structure and style}
An important part of writing easily readable code is having is a coherent structure throught for files, classes and general syntax.

\subsection{Code structure}
The student written code is located in the following files:
\begin{itemize}
\item myerbs.cpp/h - The primary class of the project where subdivision of base curves and manipulation of local curves are handled.
\item mycurve.cpp/h - Custom curve used as basis, as shown in \cref{fig::custom_curve}
\item mysubcurve.cpp/h - Subcurve implementation.. 
\item mybeziercurve.cpp/h - Bezier curve implementation. 
\item makematrix.h - creates the Bernstein Hermite polynomials and derivates, for the bezier curves. Note that this file uses a slightly different style than the rest of the code due to being written prior to this course.
\end{itemize}

Minor updates to gmlibwrapper, primarily around line 260 (define \#TEST\_ERBS), where the curves are initiated and added to the scene. 

\subsection{Coding guidelines}
The code follows common concepts accepted and used for programming languages in general and C/C++ in particular. Eg. indented blocks, separate headers and implementation, and files named after primary class that they contain. Some style choices are however down to personal preference, and for this project I have conformed to the following rules: 
\begin{itemize}
\item Newline before starting new block, including \{ .
\item Camelcase used for composite names.
\item Capitalized class names.
\item Lower case variable names.
\item Expanded variable names, unless short or single letter name is easily understandable.
\item Private and protected variables start with underscore. 
\item 4 character tabs, converted to spaces.
\end{itemize}

Sample of code style:
\begin{lstlisting}
class Example
{
    public:
        Example(double foo, double bar);
    protected:
        simpletype _privateVariableLongName;
        simpletype _cvn; //compact variable name
        Library::Class* _privateClassPointer;

}
\end{lstlisting}

\section{Tesselation}
Tesselation is the subdivision of a plane into tiles of arbitrary shapes, with no overlapping or gaps in between. In this course we have briefly touched upon Delauney and Voronoi's methods of subdision.

\begin{figure}[!ht]
\centering
\includegraphics[scale=0.5]{gfx/delauney}
\caption{Delauney triangulation (wikipedia).}
\label{fig::delauney}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[scale=0.5]{gfx/voronoi}
\caption{Voronoi diagram construction (google image search).}
\label{fig::voronoi}
\end{figure}



\section{Concluding remarks}
ERBS is a very flexible blending technique that allows for a unique approach to modelling curves and surfaces. In this project some unforseen problems with implementing the bezier curves properly prevented me from showcasing more of their potential, but there is definitely a multitude of possibilites for mathematical constructs based on these techniques.

\bibliographystyle{apalike}
\bibliography{bibliography}


\end{document} 
