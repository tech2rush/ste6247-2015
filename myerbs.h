#ifndef MYERBS_H
#define MYERBS_H

//in case of error include gmparametricsmodule (everything)
#include <parametrics/gmpcurve>
#include <parametrics/gmpsubcurve>
#include <parametrics/evaluators/gmerbsevaluator.h>
#include "mysubcurve.h"

class MyERBSCurve : public GMlib::PCurve<float,3>
{
    GM_SCENEOBJECT(MyERBSCurve)
    public:
        enum CURVE_TYPE
        {
            SUB_CURVE = 0,
            BEZIERCURVE = 1
        };

        MyERBSCurve( CURVE_TYPE type, PCurve<float,3>* curve, int n, int d = 2);
        MyERBSCurve( const MyERBSCurve& copy );

        bool isClosed() const;
        void setClosed(bool closed);

        void insertLocal( PCurve<float,3>* localCurve );
        GMlib::PCurve<float,3>* makeLocal(CURVE_TYPE type, PCurve<float,3>* curve, float start, float t, float end, int d=2);

    protected:
        int _resLocalCurves = 30;
        bool _isClosed;
        GMlib::BasisEvaluator<long double>* _evaluator;

        void eval(float t, int d = 0, bool l = false);
        float getStartP();
        float getEndP();

        float mapToLocal(float t, int tk);

        void getB( GMlib::DVector<float>& B, int k,float t, int d);
        void compBlend(int d,
                       const GMlib::DVector<float>& B,
                       GMlib::DVector< GMlib::Vector<float,3> >& c0,
                       GMlib::DVector< GMlib::Vector<float,3> >& c1);
        void localSimulate(double dt);


        GMlib::DVector<float> makeKnotVector(float start, float end, int n);

        GMlib::DVector<float> _knotVector;
        GMlib::DVector<PCurve<float,3>*> _curves;

        float _at = 0; //Accumulated time
};
#endif//MYERBS_H
