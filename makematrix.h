#ifndef MAKEMATRIX_H
#define MAKEMATRIX_H

#include <core/gmglobal>
#include <core/gmdmatrix>
#include <iostream>

template<typename T>
void makeMatrix(GMlib::DMatrix<T> &m, int d, T t, T delta)
{
    //std::cout << "Constructing matrix:\n";
    if (d<1) d =1; //Restrict intput to setDim
    m.setDim(d+1,d+1);
    //Zero-fill m
    for (int i = 0; i < m.getDim1(); i++)
        for (int j = 0; j < m.getDim2(); j++) m[i][j] = 0;

    if (d<1)
    {
        m[0][0] = 1;
        return;
    }

    //First step, bhp polynomials

    m[d-1][0] = 1 - t;
    m[d-1][1] = t;

    for(int i = d - 2; i >= 0; i--)
    {
        m[i][0] = m[i+1][0]*(1-t);
        for(int j = 1; j < d-i; j++)
        {
            //std::cout << j << std::endl;
            m[i][j] = t * m[i+1][j-1] + (1-t) * m[i+1][j];
        }
        m[i][d-i] = t * m[i+1][d-i-1];
    }

    //Verify if values are correct for first step; rows summing to one.
    for (int i = 0; i < d; i++)
    {
        float sum = 0;
        for (int j = 0; j <= d; j++) sum += m[i][j];
        if (abs(sum) - 1 > 1e-5)
        {
            std::cout << "Step one: Row " << i << " mismatch: " << sum << std::endl;
        }
        //else std::cout << "Row " << i << " ... OK" << std::endl;
    }

    //Next step, derivatives
    m[d][0] = -delta;
    m[d][1] = delta;

    for (int k = 2; k <= d; k++)
    {
        T s = k * delta;
        for (int i = d; i > (d - k); i--)
        {
            m[i][k] = s * m[i][k-1];
            for (int j = k -1; j > 0; j--)
                m[i][j] = s * (m[i][j-1] - m[i][j]);
            m[i][0] *= -s ;
        }
    }

    //Verify if values are correct for second step; rows summing to 0.
    for (int i = 1; i <= d; i++)
    {
        float sum = 0;
        for (int j = 0; j <= d; j++) sum += m[i][j];
        if (abs(sum) > 1e-5 ) std::cout << "Step two: Row " << i << " mismatch: " << sum << std::endl;
        //else std::cout << "Row " << i << " ... OK" << std::endl;
    }
}
#endif //MAKEMATRIX_H
