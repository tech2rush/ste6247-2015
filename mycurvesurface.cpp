#include "mycurvesurface.h"


MyCurveSurface::MyCurveSurface(GMlib::PCurve<float, 3> *c, float v)
{
    _c = c;
    _v = v;

}

MyCurveSurface::~MyCurveSurface()
{

}

bool MyCurveSurface::isClosedU() const
{
    return _closedU;
}

bool MyCurveSurface::isClosedV() const
{
    return _closedV;
}

void MyCurveSurface::eval(float u, float v, int d1, int d2, bool lu, bool lv)
{
    GMlib::DVector<GMlib::Vector<float,3> > f = _c->evaluate(u,d1);

    _p.setDim(d1+1, d2+1);

    _p[0][0][0] = f[0][0]*v;
    _p[0][0][1] = f[0][1]*v;
    _p[0][0][2] = f[0][2]*v;

}

float MyCurveSurface::getEndPU()
{
    return _c->getParEnd();

}

float MyCurveSurface::getEndPV()
{
    return _v;

}

float MyCurveSurface::getStartPU()
{
    return _c->getParStart();

}

float MyCurveSurface::getStartPV()
{
    return -_v;
}
